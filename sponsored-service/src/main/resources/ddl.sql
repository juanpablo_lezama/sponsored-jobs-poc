CREATE TABLE public.messages (
	id serial NOT NULL,
	message text NOT NULL,
	sender varchar(100) NOT NULL,
	contact_number varchar(100) NOT NULL,
	createdat timestamptz(0) NOT NULL,
	action varchar NOT NULL
);
CREATE TABLE public.contacts (
	id serial NOT NULL,
	name varchar(100) NOT NULL,
	phone_number varchar(100) NOT NULL,
	is_migrated varchar(1) NOT NULL DEFAULT 'N',
	text_us_id varchar(40) NULL,
	last_tr_msg text NULL
);
