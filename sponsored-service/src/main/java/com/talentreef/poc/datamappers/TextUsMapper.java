package com.talentreef.poc.datamappers;

import com.talentreef.poc.db.model.Contact;
import com.talentreef.poc.dto.textus.ContactDto;
import com.talentreef.poc.dto.textus.PhoneDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE, componentModel = "spring", injectionStrategy =
    InjectionStrategy.CONSTRUCTOR, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public class TextUsMapper {

  public ContactDto mapToContactDto (Contact contact){
    return ContactDto.builder().name(contact.getName())
        .phones(List.of(new PhoneDto(contact.getPhoneNumber(), "mobile"))).build();
  }
}
