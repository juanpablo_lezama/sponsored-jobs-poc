package com.talentreef.poc.db.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.ZonedDateTime;


@Data
@ToString
@EqualsAndHashCode
@Table(value = "messages")
public class Message {

  @Id
  @Column(value = "id")
  private Long id;

  @Column(value = "message")
  private String message;

  @Column(value= "sender")//contact:phoneNumber who sent the message
  private String sender;

  @Column(value = "contact_number")//contact: phoneNumer which receives the incoming message
  private String assignedContactNumber;

  @Column(value= "createdAt")
  private ZonedDateTime createdAt;

  @Column (value = "action")
  private String action;

}
