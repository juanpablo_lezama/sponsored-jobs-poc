package com.talentreef.poc.db.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@ToString
@EqualsAndHashCode
@Table(value = "contacts")
public class Contact {

  @Id
  @Column(value = "id")
  private Long id;

  @Column(value = "name")
  private String name;

  @Column(value= "phone_number")
  private String phoneNumber;

  @Column(value = "is_migrated")
  private String isMigrated;

  @Column(value = "text_us_id")
  private String textUsId;

  @Column(value = "last_tr_msg")
  private String transactionMessage;
}
