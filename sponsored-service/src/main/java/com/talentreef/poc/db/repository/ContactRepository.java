package com.talentreef.poc.db.repository;

import com.talentreef.poc.db.model.Contact;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends R2dbcRepository<Contact, Long> {

}
