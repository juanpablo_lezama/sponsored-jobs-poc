package com.talentreef.poc.controller;


import com.talentreef.poc.db.model.Message;
import com.talentreef.poc.event.IncomingMessageEvent;
import com.talentreef.poc.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.http.MediaType;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;

import java.util.logging.Logger;

@RestController
public class MessageController implements ApplicationListener<IncomingMessageEvent> {

  private static final Logger logger = Logger.getLogger(MessageController.class.getName());

  @Autowired
  private MessageService messageService;

  private final SubscribableChannel subscribableChannel = MessageChannels.publishSubscribe().get();

  @PostMapping("/messages/create")
  public Mono<Message> createMessage(@RequestBody Message message){
    return messageService.createMessage(message);
  }

  @CrossOrigin(value = "*")
  @GetMapping(path = "/messages")
  public Flux<Message> getAllMessages(){
    return messageService.getMessages();
  }

  @CrossOrigin(value = "*")
  @GetMapping(path = "/messages/pullIncomingMessage", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<Message> getNewMessages() {

    logger.info("called Service");

    return Flux.create(sink -> {
      logger.info("got published item.");
      MessageHandler handler =
          message -> sink.next(((Message) IncomingMessageEvent.class
              .cast(message.getPayload()).getSource()));

      sink.onCancel(() -> subscribableChannel.unsubscribe(handler));
      subscribableChannel.subscribe(handler);
    }, FluxSink.OverflowStrategy.LATEST);
  }

  @Override
  public void onApplicationEvent(IncomingMessageEvent event) {
    subscribableChannel.send(new GenericMessage<>(event));
  }
}
