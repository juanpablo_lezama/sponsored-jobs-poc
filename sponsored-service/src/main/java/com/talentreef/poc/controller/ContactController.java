package com.talentreef.poc.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.talentreef.poc.db.model.Contact;
import com.talentreef.poc.service.ContactService;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
public class ContactController {
  private final ObjectMapper objectMapper;
  private final ContactService contactService;

  public ContactController(ObjectMapper objectMapper,
                           ContactService contactService) {
    this.objectMapper = objectMapper;
    this.contactService = contactService;
  }

  @CrossOrigin(value = "*")
  @GetMapping(path = "/contacts/{id}")
  public Mono<Contact> getContact(@PathVariable Long id) {
    return contactService.getContactById(id);
  }

  @CrossOrigin(value = "*")
  @GetMapping(path = "/contacts")
  public Flux<Contact> getAllContacts() {
    return contactService.getContacts();
  }

  @CrossOrigin(value = "*")
  @PostMapping(path= "/contacts/bulkCreate")
  public Flux<Contact> postContacts(@RequestBody List<Contact> contactList){
    System.out.println("request::: "+ contactList);
    return contactService.postContacts(contactList);
  }

}
