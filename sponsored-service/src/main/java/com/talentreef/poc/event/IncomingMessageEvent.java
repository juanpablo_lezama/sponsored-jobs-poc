package com.talentreef.poc.event;

import com.talentreef.poc.db.model.Message;
import org.springframework.context.ApplicationEvent;

public class IncomingMessageEvent extends ApplicationEvent {

  /**
   * Create a new {@code ApplicationEvent}.
   *
   * @param source
   *     the object on which the event initially occurred or with
   *     which the event is associated (never {@code null})
   */
  public IncomingMessageEvent(Message source) {
    super(source);
  }
}
