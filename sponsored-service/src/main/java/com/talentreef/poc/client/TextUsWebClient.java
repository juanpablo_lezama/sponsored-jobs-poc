package com.talentreef.poc.client;

import com.google.gson.Gson;
import com.talentreef.poc.datamappers.TextUsMapper;
import com.talentreef.poc.db.model.Contact;
import io.netty.handler.ssl.SslContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import java.util.Map;


@Component
public class TextUsWebClient {

  @Autowired
  private TextUsMapper textUsMapper;

  private WebClient client = WebClient
      .builder()
      .baseUrl("https://next.textus.com")
      .clientConnector(new ReactorClientHttpConnector(
          HttpClient.create()
              .secure(spec -> spec.sslContext(SslContextBuilder.forClient())).keepAlive(false)
      ))
      .build();


  private static final String TOKEN= "Bearer eyJhbGciOiJFRDI1NTE5In0.eyJzdWIiOiI4ZjdkZjc1M2" +
      "ZjY2Q2YTE5OGRhZCIsImlhdCI6MTYxNDIxMjYxN30.OZ7V48UgfOeBLeDE4E3VJ7UXqE9d71dBVAgKObrW" +
      "PkIPB2QRnIWjF7XCbDGQRMvQq1THYE372orHTihcyCUABA";

  public Mono<Contact> postContactToTextUs(Contact contact, String accountId){
    System.out.println("sending contact to textUs :" + contact.getName());
    return client.post()
        .uri(uriBuilder -> uriBuilder
            .path("/{account}/contacts")
            .build(accountId))
        .contentType(MediaType.APPLICATION_JSON)
        .header("Authorization", TOKEN)
        .bodyValue(textUsMapper.mapToContactDto(contact))
        .accept(MediaType.APPLICATION_JSON).exchangeToMono(s-> processResponse(contact, s));
  }

  private Mono<Contact> processResponse(Contact contact, ClientResponse s) {

    if (s.statusCode() != HttpStatus.OK) {
      return s.bodyToMono(String.class)
          .map( errorMessage-> {
            contact.setTransactionMessage( s.statusCode() + " "+ errorMessage);
            contact.setIsMigrated("N");
            return contact;
      });
    }else{
      return s.bodyToMono(String.class)
          .map( jsonResponse-> {
            Gson g = new Gson();
            Map<String, Object> messageJson = g.fromJson(jsonResponse, Map.class);
            contact.setTextUsId(String.valueOf(messageJson.get("id")));
            contact.setTransactionMessage(String.valueOf(s.statusCode()));
            contact.setIsMigrated("Y");
            return contact;
          });
    }
  }


}
