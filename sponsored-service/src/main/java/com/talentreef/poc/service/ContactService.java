package com.talentreef.poc.service;

import com.talentreef.poc.client.TextUsWebClient;
import com.talentreef.poc.db.model.Contact;
import com.talentreef.poc.db.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class ContactService {

  private static final String USERNAME = "hannah-luvisi-tr";

  @Autowired
  private ContactRepository contactRepository;

  @Autowired
  private TextUsWebClient textUsWebClient;

  public Mono<Contact> getContactById(Long id){
    return contactRepository.findById(id);
  }

  public Flux<Contact> getContacts(){
    return contactRepository.findAll();
  }

  public Flux<Contact> postContacts(List<Contact> contactList){

    return Flux.fromIterable(contactList)
        .flatMap(s -> postContact(s));
  }

  public Mono<Contact> postContact(final Contact contact){
    return textUsWebClient
        .postContactToTextUs(contact, USERNAME)
        .flatMap(s -> contactRepository.save(s));
  }
}
