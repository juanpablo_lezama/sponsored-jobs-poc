package com.talentreef.poc.service;

import com.talentreef.poc.db.model.Message;
import com.talentreef.poc.db.repository.MessageRepository;
import com.talentreef.poc.event.IncomingMessageEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MessageService {

  private final ApplicationEventPublisher publisher;

  private MessageRepository messageRepository;

  public MessageService(ApplicationEventPublisher publisher, MessageRepository messageRepository){
    this.publisher= publisher;
    this.messageRepository = messageRepository;
  }

  public Mono<Message> createMessage(Message message){
    if (message.getAction().equals("message.received") ||
            message.getAction().equals("contact.opted_in") ||
            message.getAction().equals("contact.opted_out")) {
      return messageRepository.save(message)
          .doOnSuccess(sms -> this.publisher.publishEvent(new IncomingMessageEvent(sms)));
    } else{
      return messageRepository.save(message);
    }
  }

  public Flux<Message> getMessages(){
    return messageRepository.findAll();
  }
}
