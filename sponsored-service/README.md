# Sponsored Jobs Poc


Sponsored Jobs Poc Backend is a SpringBoot Java service to provide a backend interface for the newest Sponsored Jobs POC.

## Getting Started

### Prerequisites

You will need the following software at a minimum to build the project:

* Java 11+
* Gradle

### Installing

Getting your development environment running requires the installation of Java 9+.

Install Java 11+

Install Gradle 
```bash
brew install gradle 
```

## Postgres Details
The Postgres database is currently in a manual state. There is a pending Platform ticket to create the official Postgres database for this project. In the meantime we are manually creating
what we need on an existing database instance and including those scripts here in the project for use when we have an actual solution.

PostgreSQL can be download at: https://www.postgresql.org/download/

Alternative, PostgreSQL App can be download at (Mac Big Sur): https://postgresapp.com/

Once installed, a table must be created on Public squema on current user:

CREATE TABLE public.messages (
    id serial NOT NULL,
    message text NOT NULL,
    sender varchar(100) NOT NULL,
    contact_number varchar(100) NOT NULL,
    createdat timestamptz(0) NOT NULL,
    "action" varchar NOT NULL
);


## Running the Application:
To Run the application you will need the Spring Boot plugin documented [here](https://spring.io/guides/gs/spring-boot/)
After setting up or leveraging that plugin, which is part of this template, you can run the following:
```bash
$ gradle bootRun
```


## Notes:
* Controller Access to Lombok Classes 
    * Using IntelliJ, you will see a compilation error when trying to access the constructors using lombok. This happens when you do not have the lombok plugin installed.
    * Once installed, you will need to configure your settings and turn on "enable annotation processing".
    ```