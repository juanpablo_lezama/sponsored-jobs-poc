import React from 'react'

export const CheckBox = props => {
    return (
      <li>
       <input key={props.id} onChange={props.handleCheckChieldElement}
         type="checkbox"
         checked={props.isChecked}
         value={props.value} /> {props.value} - {props.phoneNumber}
      </li>
    )
}
//onChange by onClick

export default CheckBox