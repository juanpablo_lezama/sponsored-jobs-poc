import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";
import MessageList from "./MessageList"
import OptList from "./OptList"
import RequestToken from "./RequestToken"

function App() {
  return (
    <div className="App">
      <header>
        Proof of Concept - TalentReef and TextUS
      </header>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact>
            <div class="alert alert-primary">Choose a route to go to</div>
            <Link class="btn btn-outline-dark" type="button" to={{ pathname: "https://secure.indeed.com/oauth/v2/authorize?client_id=f04556d2001ae57c32341690ba78a8d69489c42a0b46f5b3201f29d9a1ef90ff&redirect_uri=http%3A%2F%2Flocalhost%2Frequesttoken&response_type=code&state=employer1234&scope=email+offline_access+employer_access" }} target="self" >
              Authorize the Partner Application
            </Link>
            <br/>
            <br/>
            <Link class="btn btn-outline-dark" type="button" to="/requesttoken">
              Request token
            </Link>
            <br/>
            <br/>
          </Route>
          <Route path="/requesttoken" component={RequestToken}></Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;