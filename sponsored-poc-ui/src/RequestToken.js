import React from "react";
import { Redirect } from "react-router-dom";
import './App.css';

class RequestToken extends React.Component {
  state = {
    redirect: null,
    isLoading: false,
    //Token Values
    step1_token: "",
    access_token_2: "",
    convid_2: "",
    refresh_token_2: "",
    scope_2: "",
    token_type_2: "",
    expires_in_2: 0,
    err_message_2: "",
    access_token_3: "",
    convid_3: "",
    refresh_token_3: "",
    scope_3: "",
    token_type_3: "",
    expires_in_3: 0,
    err_message_3: "",
    response_userinfo_4: "",
    err_message_4: ""
  };

  componentDidMount() {
     const search =this.props.location.search;
     const params = new URLSearchParams(search);
     const step1_code = params.get('code');
     this.setState({step1_token: step1_code});
  }

  postRequestToken = () => {
      var details = {
          'code': this.state.step1_token,
          'client_id': 'f04556d2001ae57c32341690ba78a8d69489c42a0b46f5b3201f29d9a1ef90ff',
          'client_secret': 'ZxFVFyXJitlgrvJRrFgrvTLKUjKgUP62puc6LOz3CectDdMVKipCbfjdwlhGNzw4',
          'redirect_uri': 'http://localhost/requesttoken',
          'grant_type': 'authorization_code',
      };

      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");

      this.setState({isLoading: true});

      // POST request using fetch with error handling
      const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: formBody
      };
      fetch('/oauth/v2/tokens', requestOptions)
        .then(async response => {
          const data = await response.json();

          // check for error response
          if (!response.ok) {
              // get error message from body or default to response status
              const error = (data && data.message) || response.status;
              return Promise.reject(error);
          }

          this.setState({access_token_2: data.access_token,
                         convid_2: data.convid,
                         refresh_token_2: data.refresh_token,
                         scope_2: data.scope,
                         token_type_2: data.token_type,
                         expires_in_2: data.expires_in,
                         err_message_2: "",
                         isLoading: false
                         })

        })
      .catch(error => {
          this.setState({err_message_2: error,
                         isLoading: false
                        })
          console.error('There was an error!', error);
      });
    }

  postRefreshToken = () => {
        var details = {
            'refresh_token': this.state.refresh_token_2,
            'client_id': 'f04556d2001ae57c32341690ba78a8d69489c42a0b46f5b3201f29d9a1ef90ff',
            'client_secret': 'ZxFVFyXJitlgrvJRrFgrvTLKUjKgUP62puc6LOz3CectDdMVKipCbfjdwlhGNzw4',
            'grant_type': 'refresh_token'
        };

        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        this.setState({isLoading: true});

        // POST request using fetch with error handling
        const requestOptions = {
          method: 'POST',
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: formBody
        };
        fetch('/oauth/v2/tokens', requestOptions)
          .then(async response => {
            const data = await response.json();

            // check for error response
            if (!response.ok) {
                // get error message from body or default to response status
                const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }

            this.setState({access_token_3: data.access_token,
                           convid_3: data.convid,
                           refresh_token_3: data.refresh_token,
                           scope_3: data.scope,
                           token_type_3: data.token_type,
                           expires_in_3: data.expires_in,
                           err_message_3: "",
                           isLoading: false
                           })

          })
        .catch(error => {
            this.setState({err_message_3: error,
                           isLoading: false
                          })
            console.error('There was an error!', error);
        });
      }

  postGetUserInfo = () => {
      this.setState({isLoading: true});

      // POST request using fetch with error handling
      const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + this.state.access_token_2
        }
      };
      fetch('/v2/api/userinfo', requestOptions)
        .then(async response => {
          const data = await response.json();

          // check for error response
          if (!response.ok) {
              // get error message from body or default to response status
              const error = (data && data.message) || response.status;
              return Promise.reject(error);
          }

          this.setState({response_userinfo_4: JSON.stringify(data),
                         err_message_4: "",
                         isLoading: false
                         })

        })
      .catch(error => {
          this.setState({err_message_4: error,
                         isLoading: false
                        })
          console.error('There was an error!', error);
      });
    }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    const {isLoading} = this.state;

    if (isLoading) {
      return <h1>Loading...</h1>;
    }

    return (
      <div class="App">
        <header>
        Indeed
        </header>

        <div class="wrapper">
          <div class="alert alert-primary" role="alert">
            Indeed Request Token Steps
          </div>
          <ul>
              <li>{' step1_token : ' + this.state.step1_token}</li>
          </ul>
          <button class="btn btn-outline-dark mr-3" type="submit" onClick={this.postRequestToken} > Step 2: Request Token </button>
          <br/>
          <br/>
          <div class="alert alert-primary" role="alert">
            Response from RequestToken
          </div>
          <ul>
             <li>{' access_token : ' + this.state.access_token_2}</li>
             <li>{' convid : ' + this.state.convid_2}</li>
             <li>{' refresh_token : ' + this.state.refresh_token_2}</li>
             <li>{' scope : ' + this.state.scope_2}</li>
             <li>{' token_type : ' + this.state.token_type_2}</li>
             <li>{' expires_in : ' + this.state.expires_in_2}</li>
             <li>{' err_message : ' + this.state.err_message_2}</li>
          </ul>
          <br/>
          <button class="btn btn-outline-dark mr-3" type="submit" onClick={this.postRefreshToken} > Step 3: Refresh Token </button>
          <br/>
          <div class="alert alert-primary" role="alert">
              Response from RefreshToken
          </div>
          <ul>
               <li>{' access_token : ' + this.state.access_token_3}</li>
               <li>{' convid : ' + this.state.convid_3}</li>
               <li>{' refresh_token : ' + this.state.refresh_token_3}</li>
               <li>{' scope : ' + this.state.scope_3}</li>
               <li>{' token_type : ' + this.state.token_type_3}</li>
               <li>{' expires_in : ' + this.state.expires_in_3}</li>
               <li>{' err_message : ' + this.state.err_message_3}</li>
          </ul>
          <br/>
          <button class="btn btn-outline-dark mr-3" type="submit" onClick={this.postGetUserInfo} > Step 4: GetUser Info </button>
          <br/>
          <div class="alert alert-primary" role="alert">
              Response from GetUserInfo
          </div>
          <ul>
               <li>{' response : ' + this.state.response_userinfo_4}</li>
               <li>{' err_message : ' + this.state.err_message_4}</li>
          </ul>
          <br/>
          <br/>
          <button class="btn btn-outline-dark" type="button" onClick={() => {this.setState({redirect: "/"});}} > Back Home </button>
          <br/>
        </div>
      </div>
    );
  }
}
export default RequestToken;