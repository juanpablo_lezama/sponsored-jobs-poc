const proxy = require('http-proxy-middleware');
module.exports = function(app) {
    app.use(proxy('/oauth/v2/tokens',
        { target: 'https://apis.indeed.com',
          changeOrigin: true}
    ));
    app.use(proxy('/v2/api/userinfo',
            { target: 'https://secure.indeed.com',
              changeOrigin: true}
    ));
}