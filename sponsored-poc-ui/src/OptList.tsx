import React, { Component } from 'react';

interface Message {
  id: number;
  message: string;
  assignedContactNumber: string;
  sender: string;
  createdAt: string;
  action: string;
}

interface MessageListProps {
}

interface MessageListState {
  messages: Array<Message>;
  isLoading: boolean;
}

class OptList extends Component<MessageListProps, MessageListState> {

  private eventSource: EventSource | undefined;

  constructor(props: MessageListProps) {
    super(props);

    this.state = {
      messages: [],
      isLoading: false
    };
  }

  buildItemList = (data: Array<Message>) => {
    let items = Array<Message>();
    data.forEach( (opt: Message) => {
      if (opt.action === 'contact.opted_in' || opt.action === 'contact.opted_out') {
        items.push(opt);
      }
    });
    return items;
  }

  async componentDidMount() {
    this.setState({isLoading: true});
    const response = await fetch('http://localhost:8080/messages');
    const data = await response.json();
    this.setState({messages: this.buildItemList(data), isLoading: false});

    this.eventSource = new EventSource('http://localhost:8080/messages/pullIncomingMessage'); 
    this.eventSource.onopen = (event: any) => console.log('open', event); 
    this.eventSource.onmessage = (event: any) => {
      const message = JSON.parse(event.data);
      if (message.action === 'contact.opted_out' || message.action === 'contact.opted_in') {
        this.state.messages.push(message);
        this.setState({messages: this.state.messages}); 
      }
    };
    this.eventSource.onerror = (event: any) => console.log('error', event);
  }

  componentWillUnmount(){
    if(this.eventSource){
      console.log("closing event Source");
      this.eventSource.close();
    }     
  }

  render() {
    const {messages, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    return (
      <div className="App">
        <header>
        Opt In/Opt Out List
        </header>
        <div className="list-group">
        {messages.map((message: Message) =>
          <a href="/" key={message.id} className="list-group-item list-group-item-action flex-column align-items-start">
          <div className="d-flex w-100 justify-content-between">
            <h5 className="mb-1">{message.message}</h5>
            <small>Account: {message.assignedContactNumber}</small>
          </div>
          <p className="mb-1">{message.sender}.</p>
          <small>{message.createdAt}.</small>
          </a>
        )}
        </div>
      </div>
    );
  }
}

export default OptList;